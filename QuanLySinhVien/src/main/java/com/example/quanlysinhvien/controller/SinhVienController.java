package com.example.quanlysinhvien.controller;

import com.example.quanlysinhvien.entity.Lop;
import com.example.quanlysinhvien.entity.SinhVien;
import com.example.quanlysinhvien.services.LopService;
import com.example.quanlysinhvien.services.SinhVienService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/sinhVien")
public class SinhVienController {

    private final SinhVienService sinhVienService;
    private final LopService lopService;

    @Autowired
    public SinhVienController(SinhVienService sinhVienService, LopService lopService) {
        this.sinhVienService = sinhVienService;
        this.lopService = lopService;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10));
    }

    @GetMapping
    public String showAllSinhVien(Model model, @RequestParam(value = "search", required = false) String search) {
        List<SinhVien> dsSinhVien;

        if (search != null && !search.isEmpty()) {
            dsSinhVien = sinhVienService.searchByTenSinhVien(search);
        } else {
            dsSinhVien = sinhVienService.getAllSinhVien();
        }

        model.addAttribute("dsSinhVien", dsSinhVien);
        return "sinhvien/list";
    }

    @GetMapping("/add")
    public String showAddSinhVien(Model model) {
        model.addAttribute("sinhVien", new SinhVien());
        List<Lop> dsLop = lopService.getAllLop();
        model.addAttribute("dsLop", dsLop);
        return "sinhvien/add";
    }

    @PostMapping("/add")
    public String addSinhVien(@Valid @ModelAttribute("sinhVien") SinhVien sinhVien, BindingResult result, Model model) {
        Lop lop = lopService.getLopById(sinhVien.getLop().getMaLop());
        sinhVien.setLop(lop);
        if(lop==null){
            return "redirect:/lop/add";
        }
        if (result.hasErrors()) {
            List<Lop> dsLop = lopService.getAllLop();
            model.addAttribute("dsLop", dsLop);
            return "sinhvien/add";
        }

        sinhVienService.addSinhVien(sinhVien);
        return "redirect:/sinhVien";
    }

    @GetMapping("/update/{mssv}")
    public String showUpdateSinhVien(@PathVariable("mssv") String mssv, Model model) {
        SinhVien sinhVien = sinhVienService.getSinhVienById(mssv);
        if (sinhVien == null) {
            return "redirect:/sinhVien/add";
        }
        List<Lop> dsLop = lopService.getAllLop();
        model.addAttribute("dsLop", dsLop);
        model.addAttribute("sinhVien", sinhVien);
        return "sinhvien/update";
    }

    @PostMapping("/update/{mssv}")
    public String updateSinhVien(@PathVariable("mssv") String mssv, @Valid @ModelAttribute("sinhVien") SinhVien sinhVien, BindingResult result, Model model) {
        Lop lop = lopService.getLopById(sinhVien.getLop().getMaLop());
        sinhVien.setLop(lop);
        if(lop==null){
            return "redirect:/lop/add";
        }
        if (result.hasErrors()) {
            List<Lop> dsLop = lopService.getAllLop();
            model.addAttribute("dsLop", dsLop);
            return "sinhvien/update";
        }
        SinhVien existingSinhVien = sinhVienService.getSinhVienById(mssv);
        if (existingSinhVien == null) {
            return "redirect:/sinhVien/add";
        }
        sinhVienService.updateSinhVien(sinhVien);
        return "redirect:/sinhVien";
    }

    @GetMapping("/delete/{mssv}")
    public String deleteSinhVien(@PathVariable("mssv") String mssv) {
        SinhVien sinhVien = sinhVienService.getSinhVienById(mssv);
        if (sinhVien == null) {
            return "redirect:/sinhVien/add";
        }
        sinhVienService.deleteSinhVien(mssv);
        return "redirect:/sinhVien";
    }
}
