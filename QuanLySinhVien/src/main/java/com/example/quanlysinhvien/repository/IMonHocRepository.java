package com.example.quanlysinhvien.repository;

import com.example.quanlysinhvien.entity.MonHoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface IMonHocRepository extends JpaRepository<MonHoc, String> {
}
